﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour {
	public bool automaticPlay = false;
	private Ball ball;
	void Start(){
		ball = GameObject.FindObjectOfType<Ball>();
	}
	void Update () {
		if(!automaticPlay){
			MoveWithMouse();
		}else{
			AutoPlay();
		}
	}

	void MoveWithMouse(){
		float mousePosInBlocks = Input.mousePosition.x / Screen.width * 16;
		Vector3 paddlePos = new Vector3 (Mathf.Clamp (mousePosInBlocks,0.5f,15.5f),this.transform.position.y,this.transform.position.z);
		this.transform.position = paddlePos;
	}

	void AutoPlay(){
		Vector3 ballPos = new Vector3 (Mathf.Clamp (ball.transform.position.x,0.5f,15.5f),this.transform.position.y,this.transform.position.z);
		this.transform.position = ballPos;
	}
}
