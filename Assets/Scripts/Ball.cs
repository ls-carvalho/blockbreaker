﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {
	private Paddle myPaddle;
	private Vector3 paddleToBall;
	private bool hasStarted = false;
	private Brick bGame;
	//private LevelManager levelManager; //Was Used on the old win condition

	void Start () {
		//levelManager = GameObject.FindObjectOfType<LevelManager>(); //Was Used on the old win condition
		myPaddle = GameObject.FindObjectOfType<Paddle>();
		paddleToBall = this.transform.position - myPaddle.transform.position;
	}

	void Update () {
		if(!hasStarted){
			this.transform.position = myPaddle.transform.position + paddleToBall;
			if(Input.GetMouseButtonDown(0)){
				hasStarted = true;
				float rand = Random.Range(-3,3);
				this.rigidbody2D.velocity = new Vector2 (rand, 10f);
			}
		/*}else{ //Old win condition, remaked on "LevelManager" script
			bGame = GameObject.FindObjectOfType<Brick>();
			if(bGame!=null){
				LevelManager.WinCondition(true);
			}*/
		}
	}

	void OnCollisionEnter2D(Collision2D collision){
		Vector2 tweak;
		if(hasStarted){
			audio.Play();
			if(rigidbody2D.velocity.x==0f){
				tweak = new Vector2 (Random.Range(0f,3f),0f);
				rigidbody2D.velocity += tweak;
			}else if(rigidbody2D.velocity.y==0f){
				tweak = new Vector2 (0f,Random.Range(0f,3f));
				rigidbody2D.velocity += tweak;
			}
		}
	}
}
