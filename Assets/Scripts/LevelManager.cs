﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public void LoadLevel(string name){
		Application.LoadLevel(name);
		ResetBrick();
	}

	public void LoadNextLevel(){
		Application.LoadLevel(Application.loadedLevel+1);
		ResetBrick();
	}

	public void QuitRequest(){
		Application.Quit();
	}

	public void BlockDestroyed () {
		if(Brick.breakableCount<=0){
			LoadNextLevel();
		}
	}

	public void ResetBrick () {
		if(Brick.breakableCount!=0){
			Brick.breakableCount=0;
		}
	}
}
