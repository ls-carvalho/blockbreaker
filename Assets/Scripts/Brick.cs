﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour {
	public static int breakableCount = 0;
	private int timesHit;
	public Sprite[] sprites;
	private bool isBreakable = false;
	private LevelManager levelManager;
	public AudioClip crack;
	public GameObject smoke;

	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager>();
		timesHit = 0;
		if(string.Equals("Breakeable",this.tag)){
			isBreakable = true;
			breakableCount++;
		}
	}

	void OnCollisionEnter2D(Collision2D collision){
		if(isBreakable){
			timesHit++;
			AudioSource.PlayClipAtPoint(crack,transform.position, 0.5f);
		}
	}

	void OnCollisionExit2D(Collision2D collision){
		if(isBreakable){
			HandleHits();
		}
	}

	void HandleHits(){
		int maxHits = sprites.Length +1;
		if(timesHit>=maxHits){
			breakableCount--;
			levelManager.BlockDestroyed();
			smoke = Instantiate(smoke, gameObject.transform.position, Quaternion.identity) as GameObject;
			smoke.particleSystem.startColor = this.GetComponent<SpriteRenderer>().color;
			Destroy(smoke,smoke.particleSystem.duration);
			Destroy(gameObject);
		}else{
			LoadSprites();
		}
	}

	void LoadSprites () {
		int spriteIndex = timesHit-1;
		if(sprites[spriteIndex]){
			this.GetComponent<SpriteRenderer>().sprite = sprites[spriteIndex];
		}
	}
}
